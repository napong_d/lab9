package Lab9;

public interface State {

	/**
	 * @param c is character that want to check
	 */
	public void handleChar ( char c);

}
