package Lab9;

import java.net.MalformedURLException;
import java.net.URL;

public class Main {
	public static void main(String[] args){
		WordCounter wc = new WordCounter();
		StopWatch timer = new StopWatch();
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		try{
			URL url = new URL( DICT_URL );
			timer.start();
			System.out.println(wc.countWords(url));
			timer.stop();
			System.out.println(wc.totalSyllables);
			System.out.println(timer.getElapsed());
		}
		catch(MalformedURLException e){
		}
	}
}
