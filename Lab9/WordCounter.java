package Lab9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;
/**
 * WordCounter count the word
 * @author Napong D
 *
 */
public class WordCounter {
	private State state;
	private int total = 0;
	protected int totalSyllables = 0;

	/**
	 * countSyllables count word
	 * @param word
	 * @return total of word
	 */
	public int countSyllables( String word ){
		total = 0;
		state = startState;
		for(char c : word.toCharArray()){
			state.handleChar(c);
		}
		if(state == dashState){
			total = 0;
		}
		else if (state == eState && total != 1){
			total--;
		}

		return total;
	}

	/**
	 * Count all words read from an input stream an return the total count.
	 * @param instream is the input stream to read from.
	 * @return number of words in the InputStream.  -1 if the stream
	 *        cannot be read.
	 */

	public int countWords(URL url) {
		int total = 0;
		try {
			InputStream in = url.openStream( );
			BufferedReader breader = new BufferedReader( new InputStreamReader(in) );
			while( true ) {
				String line = breader.readLine( );
				if (line == null) break;
				totalSyllables += countSyllables(line);
				total++;

			}
		} catch (IOException e) {
		}
		return total;
	}

	/**
	 *isVowel check is vowel
	 * @param c
	 * @return vowel
	 */
	public boolean isVowel(char c){
		String vowel = "AEIOUaeiou";
		String temp = c + "";
		return vowel.contains(temp);
	}

	/**
	 * consonantState check handle char
	 */
	State consonantState = new State(){
		public void handleChar( char c ){
			if ( c == 'e' || c== 'E'){
				total++;
				state = eState;
			}
			else if( isVowel(c) || c == 'y' || c == 'Y'){
				total++;
				state = vowelState;
			}
			else if (Character.isLetter(c)){
				state = consonantState;
			}
			else if( c == '-' )
				state = dashState;
			else if ( c== '\'')
				state = consonantState;
			else 
				state = nonWordState;
		}
	};

	/**
	 * vowelState check vowel
	 */
	State vowelState = new State() {
		public void handleChar( char c ){
			if(isVowel(c)){
				state = vowelState;
			}
			else if( Character.isLetter( c ) && !(isVowel( c ) ) )
				state = consonantState;
			else if( c == '-' )
				state =  dashState;
			else 
				state = nonWordState;
		}
	};

	/**
	 * dashState check dash
	 */
	State dashState  = new State(){
		public void handleChar( char c ){
			if ( isVowel( c ) ){
				total++;
				state = vowelState;
			}
			else if(Character.isLetter(c))
				state = consonantState;
			else 
				state = nonWordState;
		}
	};

	/**
	 * eState check character e 
	 */
	State eState = new State(){
		public void handleChar (char c){
			if(isVowel(c))
				state = vowelState;
			else if(Character.isLetter(c))
				state = consonantState;
			else if(c=='-')
				state = dashState;
			else 
				state = nonWordState;
		}
	};

	/**
	 * nonWordState check nonword
	 */
	State nonWordState = new State(){
		public void handleChar(char c){
			total = 0;
		}
	};


	/**
	 * startState check start
	 */
	State startState = new State() {
		public void handleChar ( char c ){
			if ( c == 'e' || c == 'E'){
				total++;
				state = eState;
			}
			else if(isVowel(c) || c == 'y' || c == 'Y'){
				total++;
				state = vowelState;
			}
			else if( Character.isLetter(c))
				state = consonantState;
			else 
				state = nonWordState;
		}
	};
}
